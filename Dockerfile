FROM openjdk:11-ea-11-jre-slim
VOLUME /tmp
ADD /target/prometheus-boot-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8082
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
